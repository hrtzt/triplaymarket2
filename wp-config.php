<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
define("WPLANG", "es_ES");

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'alumin_triplaymarket_2' );

/** MySQL database username */
define( 'DB_USER', 'local' );

/** MySQL database password */
define( 'DB_PASSWORD', 'local' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '##Okq-xPi):2NYP*u6)A^5*bk,VN_,[psV^Wyq>-l58;L|dn_9[ePa-_>i#`{93G' );
define( 'SECURE_AUTH_KEY',  '6Vc?3l2?+eimS!Y7_B92S.Yjko%i|i(:&T+K..8#iXJ8Q[y:06?K.W&O94_@#xOR' );
define( 'LOGGED_IN_KEY',    'Dw)5`Q.bj?XO8~#&}`wn_2s/zCR`:oA:6G<{8S.Z@tKnG!A}g2yG[^?D@b3}EDP2' );
define( 'NONCE_KEY',        'gp58(8(j5_:J)7Tf1Ck]D_)z0k`p=0--z9xGvC}5zPP#Fa?zV6?+d_r:EhL!yM%I' );
define( 'AUTH_SALT',        'HPlf9c&3fstnP(y#8tTFO@Do7;}`)j.a56q#LyMjVof{DCy0_{ycdj6]o(RGD{&Q' );
define( 'SECURE_AUTH_SALT', '(V@AXe8hGC`x89dz}A`=aVj[3LBTQm[%!`<JdeyF|<#~b8Qu`urE_5sh(yxc<1.K' );
define( 'LOGGED_IN_SALT',   'DXYxF4n.nNb@FGH7>B^_liPb ;<whqmHU%/Iw-/(;#ug>0}J{U&,%A.^4e&=P8bW' );
define( 'NONCE_SALT',       '$Ao!U0DG}>5{z<aHRm.0=tXh=H}lQ94G~;NCng:bLMqn8b?5(ftMK#>|CeW)8dpy' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'tpxy_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */



/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
