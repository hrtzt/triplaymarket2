<section id="absolute" class="absolute">

    <div class="wk-section-wrap">

        <div class="wk-cols-me absolute__cols">
        
            <div class="wk-col absolute__col">
    
                <div class="absolute__content absolute__content--left">
                    <ul class="absolute__menu">
                        <?php if( get_field( 'info_facebook', 'option' ) ) : ?>
                            <li class="absolute__menu__item"><a target="_blank" href="<?php the_field( 'info_facebook', 'option' ); ?>"><span class="fab fa-facebook-f"></span></a></li>
                        <?php endif; ?>
                        <?php if( get_field( 'info_facebook', 'option' ) ) : ?>
                            <li class="absolute__menu__item"><a target="_blank" href="<?php the_field( 'info_instagram', 'option' ); ?>"><span class="fab fa-instagram"></span></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
    
            </div>
        
            <div class="wk-col absolute__col">
    
                <div class="absolute__content absolute__content--right">
                    <ul class="absolute__menu">
                        <?php if( get_field( 'info_tel_main', 'option' ) ) : ?>
                            <li class="absolute__menu__item"><a href="tel:<?php the_field( 'info_tel_main' , 'option'); ?>"><span class="aboslute__menu-icon fab fa-whatsapp"></span> <span><?php the_field( 'info_tel_main' , 'option'); ?></span></a></li>
                        <?php endif; ?>
                        <li class="absolute__menu__item"><a id="searchform__icon" href="javascript:;" data-fancybox data-src="#searchform"><span class="fas fa-search"></span></a></li>
                    </ul>
                </div>
    
            </div>
        
        </div>

    </div>


</section>