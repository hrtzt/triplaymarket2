$('.slider-brand-strip').slick({
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 4,
    centerMode: false,
    focusOnSelect: true,
    dots: true,
    rows: 0,
    prevArrow: '<span class="slick-prev"><span class="slick-icon"></span></span>',
    nextArrow: '<span class="slick-next"><span class="slick-icon"></span></span>',
});