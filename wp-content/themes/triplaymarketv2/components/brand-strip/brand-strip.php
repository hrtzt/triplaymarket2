<section id="brand-strip" class="brand-strip wk-section">
    <div class="wk-section-wrap">

        <ul class="slider-brand-strip">

            <?php if( have_rows( 'home_marcas' ) ) : while( have_rows( 'home_marcas' ) ) : the_row(); ?>

                <li>
                    <a href="<?php the_sub_field( 'home_marca_link' ); ?>" target="_blank">
                        <img class="brand-strip__image" src="<?php the_sub_field( 'home_marca_thumb' ); ?>" alt="Arauco">
                        <img class="brand-strip__logo" src="<?php the_sub_field( 'home_marca_logo' ); ?>" alt="Logo">
                    </a>
                </li>

            <?php endwhile; endif; ?>

        </ul>

    </div>
</section>