<div class="wk-cols sucursal">

    <div class="wk-col estado">

        <strong><?php $estados = wp_get_post_terms( $post->ID, 'estados' ); foreach( $estados as $estado ) : echo $estado->name; endforeach; ?></strong>

    </div>

    <div class="wk-col local">

        <span> <?php the_title(); ?></span>

    </div>

    <div class="wk-col direccion">

        <div>
            <?php the_field( 'directorio_direccion' ); ?><br>

            <?php if( have_rows( 'directorio_telefonos' ) ) : while( have_rows( 'directorio_telefonos' ) ) : the_row(); ?>
                <?php if( get_sub_field( 'directorio_prefijo' ) ) : ?><?php the_sub_field( 'directorio_prefijo' ); ?>"> <?php endif; ?>
                <a href="tel:<?php the_sub_field( 'directorio_telefono' ); ?>"><?php the_sub_field( 'directorio_telefono' ); ?></a>
            <?php endwhile; endif; ?>
        </div>

    </div>

    <div class="wk-col servicios">

        <div class="wk-cols servicios-container">

            <span class="wk-col servicio">

                <?php if( get_field( 'directorio_servicios_de_corte' ) ) : ?>
                    <span class="checked fa fa-star-half"></span>
                    <span class="checked-label">Servicios de corte</span>
                <?php endif; ?>

            </span>

            <span class="wk-col servicio">

                <?php if( get_field( 'directorio_corte45' ) ) : ?>
                    <span class="checked fa fa-cube"></span>
                    <span class="checked-label">Corte 45 cubiertas de cocina</span>
                <?php endif; ?>

            </span>

            <span class="wk-col servicio">

                <?php if( get_field( 'directorio_entrega_a_domicilio' ) ) : ?>
                    <span class="checked fa fa-truck"></span>
                    <span class="checked-label">Entrega a domicilio</span>
                <?php endif; ?>

            </span>

            <span class="wk-col servicio">

                <?php if( get_field( 'directorio_cepillado' ) ) : ?>
                    <span class="checked fa fa-industry"></span>
                    <span class="checked-label">Cepillado</span>
                <?php endif; ?>
                
            </span>

            <span class="wk-col servicio">

                <?php if( get_field( 'directorio_estacionamiento' ) ) : ?>
                    <span class="checked fa fa-car"></span>
                    <span class="checked-label">Estacionamiento</span>
                <?php endif; ?>

            </span>

            <span class="wk-col servicio">

                <?php if( get_field( 'directorio_pago_con_targeta' ) ) : ?>
                    <span class="checked fa fa-credit-card"></span>
                    <span class="checked-label">Pago con tarjeta</span>
                <?php endif; ?>
                
            </span>

        </div>

    </div>
    
</div>