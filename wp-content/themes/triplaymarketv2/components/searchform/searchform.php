<section id="searchform" class="wk-section" style="display: none;">
    <div class="wk-section-wrap">

        <form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
            <input class="searchform__inpu searchform__input-text" type="text" value="<?php the_search_query(); ?>" name="s" id="s" />
            <input class="searchform__inpu searchform__input-button" type="submit" id="searchsubmit" value="Buscar" />
        </form>

    </div>
</section>