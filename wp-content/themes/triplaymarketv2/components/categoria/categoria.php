<?php

    if( is_product_category() ) {
        $product_cat = get_queried_object();
		$product_catID = $product_cat->term_id;
        $args = array(
            'taxonomy'      => 'product_cat',
            // 'child_of'   => '0',
            // 'childless'  => false,
            'parent'        => $product_catID,
            'exclude'       => '16',
            'hide_empty'    => 0,
            // 'number'        => 10
        );
    } elseif( is_front_page() ) {
        $args = array(
            'taxonomy'      => 'product_cat',
            // 'child_of'   => '0',
            // 'childless'  => false,
            // 'parent'     => $product_catID,
            // 'exclude'       => array(16),
            'hide_empty'    => 0,
            // 'number'        => 22
        );
    } else {
        $args = array(
            'taxonomy'      => 'product_cat',
            // 'child_of'   => '0',
            // 'childless'  => false,
            // 'parent'     => $product_catID,
            // 'exclude'       => '16',
            'hide_empty'    => 0,
            // 'number'        => 2
        );

    }

    // print_r($product_cat);


    $all_categories = get_categories( $args ); 

        // print_r($all_categories);
        
    $contadorS = 0;
        
?>

<?php foreach($all_categories as $cat) : $contadorS++ ?>

    <?php if( $cat->category_parent > 0 ) : ?>

        <?php 
        
            // print_r($cat);
                        
            global $woocommerce;  
            
            //Imprime la clase .categoria--rtl a bloques pares
            if( $contadorS % 2 == 0 ) {
                $catRTL = 'categoria--rtl';
            } else {
                $catRTL = null;
            }
        ?>
    
        <section class="categoria <?php echo $cat->slug; ?> wk-section <?php echo $catRTL; ?> categoria-row-<?= $contadorS ?>">
    
            <div class="wk-section-wrap">
            
                <div class="categoria-top">
    
                        <div class="wk-cols categoria__cols">
                        
                            <div class="wk-col-3 categoria__col categoria__col--left">

                                <h1 class="categoria__cat-label"><?= get_the_category_by_ID( $cat->category_parent ); ?> <i style="font-size: 9px;" class="fas fa-chevron-right"></i> <span><?= $cat->name; ?></span></h1><br>
                    
                                <?php 
                                    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
                                    $catImage = wp_get_attachment_image_src( $thumbnail_id, 'medium' ); 
                                ?>
    
                                <?php if( $catImage ) : ?>
                                    
                                    <img class="categoria__img categoria__img-brand" src="<?php echo $catImage[0]; ?>" alt="">
    
                                <?php else : ?>
                                        
                                    <h1 class="ui-title"><?= $cat->name; ?></h1>
    
                                <?php endif; ?>
    
                                <p><?php //echo $cat->description; ?></p>

                                <div class="categoria__description">
                                    <?php echo get_field( 'cat_desc', 'term_' . $cat->term_id ); ?>
                                </div>

                                <p>
                                    <span>Disponibles en:&nbsp;</span>
                                    <strong class="categoria__presentacion">  
                                        <?php 

                                            $args= array(
                                                    'status'        => 'publish',
                                                    'order'         => 'ASC',
                                                    'category'      => array( $cat->category_nicename ),
                                                    'posts_per_page'=> -1
                                                );

                                            $products = wc_get_products( $args ); 
    
                                            // Obtiene los valores de el attributo presentacion pa_presentacion
    
                                            $attr_presentacion_merge = [];
                                        
                                            foreach( $products as $product ) {
                                            
                                                if($product->is_type('variable')) {
                                                    // Obtiene todos los atributos de las variaciones del producto
                                                    $variation_attr = $product->get_variation_attributes(); 
        
                                                    // Obtiene solo los valores del atributo pa_presentacion
                                                    $attr_presentacion = $variation_attr['pa_presentacion'];

                                                    if( $attr_presentacion ) {
            
                                                        // Mezcla cada arreglo de cada producto del atributo pa_presentacion
                                                        $attr_presentacion_merge = array_merge($attr_presentacion_merge, $attr_presentacion);

                                                    }
                                                } 

                                            }
                                            
                                            // Elimina los valores repetidos en el arreglo
                                            $presentacion_unique = array_unique( $attr_presentacion_merge );
    
                                            // Obtiene el tamaño del arreglo para despúes hacer un foreach
                                            $presentacion_unique_size = sizeof( $presentacion_unique );
    
                                            $p_contador = 1;
    
                                            // Obtiene todos los valores del arreglo final
                                            foreach ( $presentacion_unique as $presentacion_values ) {
                                                $p_contador++;
                                                echo strtoupper( $presentacion_values );
    
                                                // Agrega "," o "y" según la posición en el arreglo.
                                                if( $p_contador < $presentacion_unique_size ) {
                                                    echo ', ';
                                                } elseif( $p_contador == $presentacion_unique_size ) {
                                                    echo ' y ';
                                                }
                                            }                                        
    
                                        ?>
                                    </strong>
                                </p>
                    
    
                                <?php if(get_field( 'cat_ficha_tecnica', 'term_' . $cat->term_id ) ) : ?>
                                    
                                    <p class="categoria__ficha-tecnica">
                                        <a class="ui-label-text" href="<?php echo get_field( 'cat_ficha_tecnica', 'term_' . $cat->term_id ); ?>" target="_blank">
                                            <strong>Descarga ficha técnica <?php echo $cat->name; ?></strong> 
                                            <span class="categoria__ficha-tecnica__icon fa fa-download"></span>
                                        </a>
                                    </p>
    
                                <?php endif; ?>
                    
                            </div>
                        
                            <div class="wk-col-5 categoria__col categoria__col--right">
    
                                <?php 
                                    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
                                    $image = wp_get_attachment_image_src( $thumbnail_id, 'medium' ); 
                                ?>

                                <?php if( $image ) : ?>
                                    <img class="categoria__img" src="<?php echo get_field('cat_featured_img', 'term_' . $cat->term_id); echo $image['url']; ?>" alt="Thumbnail">
                                <?php else : ?>
                                    <div class="categoria__img_default-container">
                                        <img class="categoria__img categoria__img_default" src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnail-default-branded2.png" alt="Thumbnail">
                                    </div>
                                <?php endif; ?>

                                <p class="categoria__catalogo-title">Catálogo <?php echo $cat->name; ?></p>
                                <?php if( $products ) : ?>
                                    <p class="categoria__help-text">Selecciona una muestra para ver sus opciones</p>
                                    <ul class="categoria__samples">
                                        
                                        <?php $odd_even_counter = 0; ?>
                                        <?php //print_r($products); ?>
                                        <?php foreach( $products as $product ) : ?>
                                            
                                            <?php
                                                $odd_even_counter++;
                                                if( $odd_even_counter % 2 == 0 ) {
                                                    $odd_even = 'odd';
                                                } else {
                                                    $odd_even = 'even';
                                                }
                                            ?>
                                            <?php $data = $product->get_data(); ?>
                                            <li class="categoria__sample" data-variacion="<?= $data['name']; ?>" data-title="<?= $data['name']; ?>">
                                                <a href="javascript:;" data-src="#variations-form-<?php echo $product->get_id(); ?>" data-fancybox="product-<?= $cat->category_nicename ?>" >
                                                    <?php if( wp_get_attachment_url($product->get_image_id()) ) : ?>
                                                        <?php echo wp_get_attachment_image( $product->get_image_id(), 'icon', false, array( 'class' => 'categoria__sample-img' ) ); ?>
                                                    <?php else : ?>
                                                        <span class="categoria__sample-default categoria__sample-default_<?= $odd_even ?>">
                                                            <i class="far fa-image"></i>
                                                        </span>
                                                    <?php endif; ?>
                                                </a>
                                            </li>

                                            <?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

                                            <div id="variations-form-<?php echo $product->get_id(); ?>" class="variations-form__wrapper" style="display: none">

                                                <?php // edit_post_link('<span class="fas fa-edit"></span> Editar producto'); ?>

                                                <?php //print_r($data); ?>
                                                <h2 class="variations-form__subtitle"><?php echo $cat->name; ?></h2>
                                                <h1 class="variations-form__title"><?= $data['name']; ?></h6>
                                                <article class="variations-form__description">
                                                    
                                                    <?= $data['short_description']; ?>
                                                    <?php //print_r($product) ?>

                                                </article>

                                                <?php if($product->is_type('variable')) : ?>
                                            
                                                    <?php

                                                        $available_variations = $product->get_available_variations(); 
                                                        $attributes = $product->get_attributes();
                                                        $variation_attributes = $product->get_variation_attributes();
                                                        $attribute_keys  = array_keys( $variation_attributes );
                                                        $variations_json = wp_json_encode( $available_variations );
                                                        $variations_attr = function_exists( 'wc_esc_json' ) ? wc_esc_json( $variations_json ) : _wp_specialchars( $variations_json, ENT_QUOTES, 'UTF-8', true );

                                                        do_action( 'woocommerce_before_add_to_cart_form' ); 
                                                    
                                                    ?>

                                                <?php endif; ?>

                                                <?php //print_r($attributes); ?>

	                                            <form class="variations_form cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->get_id() ); ?>" data-product_variations="<?php echo $variations_attr; // WPCS: XSS ok. ?>">
                                                    
                                                    <?php do_action( 'woocommerce_before_variations_form' ); ?>

                                                        <?php 
                                                                
                                                            /*
                                                            /* Los productos con la clase de envío "Solicitar más información en sucursal"
                                                            /* $shipping_class_id = [id de la clase Solicitar más información en sucursal]
                                                            /* no muestran las opciones de variaciones.
                                                            */

                                                            $shipping_class_id = 67;

                                                        ?>
                                                    
                                                        <div class="variations-form-container <?= $data['shipping_class_id'] == $shipping_class_id ? 'variations-form-container_layout-column' : '' ?>">

                                                            <?php if ( empty( $available_variations ) && false !== $available_variations ) : ?>
                                                                <p class="stock out-of-stock"><?php esc_html_e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); ?></p>
                                                            <?php else : ?>

                                                                <?php $gallery_images = $product->get_gallery_image_ids(); ?>

                                                                <div class="variations-form__data variations-form__data-image">

                                                                    <?php if( wp_get_attachment_url($product->get_image_id()) ) : ?>
                                                                        <img <?= $gallery_images ? 'data-fancybox="images" data-src="' . wp_get_attachment_url( $product->get_image_id(), 'full' ) . '" href="javascript:;"' : '' ?> class="categoria__sample-img" src="<?= wp_get_attachment_url($product->get_image_id()) ?>" alt="<?= $data['name']; ?>">
                                                                    <?php else : ?>
                                                                        <span class="categoria__sample-default categoria__sample-default_<?= $odd_even ?>">
                                                                            <i class="far fa-image"></i>
                                                                        </span>
                                                                    <?php endif; ?>

                                                                    <?php
                                                                        
                                                                        if($gallery_images) {
                                                                            foreach( $gallery_images as $gallery_image ) { 
                                                                                ?>
                                                                                    <a class="variations-form__description-gallery-img-link" href="javascrip:;" data-fancybox="images" data-src="<?= wp_get_attachment_image_url( $gallery_image, 'full' ) ?>">
                                                                                        <img class="variations-form__description-gallery-img" src="<?= wp_get_attachment_image_url( $gallery_image, 'thumbnail' ) ?>" alt="<?= $data['name']; ?>">
                                                                                    </a>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        
                                                                    ?>

                                                                </div>

                                                                <?php if( ! $data['shipping_class_id'] == $shipping_class_id ) : // 67 es la clase de envio "Solicitar más información en sucursal" ?>

                                                                    <?php if($product->is_type('variable')) : ?>

                                                                    <?php 
                                                                    
                                                                        // print_r( $product ); 
                                                                    
                                                                        // print_r( $variations_json );
                                                                        
                                                                    
                                                                    ?>

                                                                    <div class="variations variations-form__variations">
                                                                        
                                                                        <?php foreach( $variation_attributes as $attribute_name => $options ) : ?>


                                                                            <div class="variations-form__variation variations-form__data">

                                                                                <div class="variations-form__variation-label variations-form__label label"><?php echo wc_attribute_label( $attribute_name ); // WPCS: XSS ok. ?></div>
                                                                                <div class="variations-form__variation-value variations-form__value value">

                                                                                    <?php
                                                                                        wc_dropdown_variation_attribute_options( array(
                                                                                            'options'           => $options,
                                                                                            'attribute'         => $attribute_name,
                                                                                            'product'           => $product,
                                                                                            'name'              => '',  
                                                                                            'id'                => $product->slug . '-' .$attribute_name,  
                                                                                            'class'             => 'attr_select',
                                                                                            'show_option_none' => __( 'Selecciona una opción', 'wpkit' ),  
                                                                                        ) );
                                                                                        echo end( $attribute_keys ) === $attribute_name ? wp_kses_post( apply_filters( 'woocommerce_reset_variations_link', '<a id="cclear" class="reset_variations" href="#">Borrar opciones</a>' ) ) : '';
                                                                                    ?>

                                                                                </div>

                                                                            </div>

                                                                        <?php  endforeach; ?>

                                                                    </div>

                                                                    <?php endif; ?>
                                                                
                                                                    <div class="single_variation_wrap variations-form__data">
                                                                        <div class="woocommerce-variation-add-to-cart variations_button woocommerce-variation-add-to-cart-enabled">
                                                                            <div class="quantity ">
                                                                                <label class="variations-form__label screen-reader-text" for="quantity_5d2a845634ce4">Cantidad</label>
                                                                                <input type="number" id="quantity-<?= $product->slug ?>" class="variations-form__value input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="Qty" size="4" inputmode="numeric">
                                                                            </div>
                                                                            
                                                                        </div>
                                                                    </div>

                                                                <?php else : ?>

                                                                    <p class="variations-form__noquote-message">Para más información solicita información en tu <a data-fancybox-close class="variations-form__close-lightbox" href="<?php bloginfo( 'url' ); ?>/#section-sucursales">sucursal más cercana</a></p>

                                                                <?php endif; ?>

                                                            <?php endif; ?>

                                                            
                                                        </div>
                                                
                                                        <?php if( ! $data['shipping_class_id'] == $shipping_class_id ) : // 55 es la clase de envio "Solicitar más información en sucursal" ?>
                                                            <button type="submit" class="ui-button single_add_to_cart_button button alt">Añadir a la cotización</button>
                                                            <input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>">
                                                            <input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>">
                                                            <input type="hidden" name="variation_id" class="variation_id" value="0">
                                                        <?php endif; ?>

                                                        <?php do_action( 'woocommerce_after_variations_form' ); ?>

                                                </form>

                                                <?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

                                            </div>
                                            <?php //echo $data['name']; ?><br>
                                        <?php endforeach; ?>
                                    
                                    </ul>       
                                <?php else : ?>
                                    <p>Aún no hay productos.</p>
                                <?php endif; ?>
                    
                            </div>
                        
                        </div>
    
                </div>
    
                <!-- <div class="categoria-bottom">
    
                    <div class="wk-cols categoria__cols">
    
                        <div class="wk-col-3 categoria__col categoria__col--left"> </div>
    
                        <div class="wk-col-5 categoria__col categoria__col--right"> </div>
    
                    </div>
    
                </div> -->
            
            </div>
    
        </section>

    <?php endif; ?>


<?php endforeach; ?>