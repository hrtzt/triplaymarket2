

$(window).on('load', function () {
    $('body').addClass('testeando');
    // $('.woocommerce-notices-wrapper').css('opacity', '1');
    $('.woocommerce-notices-wrapper').animate({
        // Visibility solo como placeholder para
        // que después entre 'step'
        visibility: 'visible',
    }, {
            step: function (now, fx) {
                $(this).css('-webkit-transform', 'translateY(0)');
                $(this).css('opacity', '1');
            }
        }, 900, function () {
            console.log('algo');
        });
    setTimeout(function () {
        $('.woocommerce-notices-wrapper').css('opacity', 0);
    }, 4000);

});

/*
* Muestra y oculta el botón para limpiar las opciones
* del dropdown de select en la forma de variaciones (añadir al carrito)
* Ocultarlo depnde solo del elemento select en variations form
* Mostrarlo depende de la clase .attr_select añadida en components/categoria.php
* 
* Esto solo es necesario para las páginas con el query de categoria.php
* porque no carga los scritps de woocomerce
* Para single product de woocomerce usa las funciones nativas de wc
*
*/

$('.reset_variations').css('visibility', 'hidden');

$('.reset_variations').click(function () {
    var var_form = $(this).parents('.variations-form');
    var select = var_form.find('select');
    select.val(null).trigger('change');
    $(this).css('visibility', 'hidden').trigger('change');
    
});

$('.attr_select').change(function(){
    var var_form = $(this).parents('.variations-form');
    var_form.find('.reset_variations').css('visibility', 'visible');
});

/* 
 * Evento en cambio de valores del select de variaciones 
 */

// $(".variations_form").on("woocommerce_variation_select_change", function () {
//     alert('alog 1');
// });

// $(".single_variation_wrap").on("show_variation", function (event, variation) {
//     alert('alog 2');
//     // Fired when the user selects all the required dropdowns / attributes
//     // and a final variation is selected / shown
// });