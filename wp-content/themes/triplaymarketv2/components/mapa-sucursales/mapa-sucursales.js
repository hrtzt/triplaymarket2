$('.estado').click(function () {
    let nombre = $(this).attr("name");
    let slug = $(this).attr("data-estado");
    // console.log(nombre);
    $('#estado-data').text(nombre);

    let data_slug = '[data-estado="' + slug + '"]';

    $('.mapa-data').find(data_slug).addClass('active');
    $('.mapa-data-item').not(data_slug).removeClass('active');


});

$('.mapa-data-item').each(function () {
    var estado = $(this).attr('data-estado');
    var svg_item = '.mapa-sucursales [data-estado="' + estado + '"]';
    $(svg_item).attr('data-exist', 'true');
});
