<!-- Esto es solo un template, no modifiques ni uses este archivo, copialo en donde lo requieras -->

<section id="main-video" class="wk-section main-video">

    <div class="wk-section-wrap">
        
        <a class="video" href="https://www.youtube.com/watch?v=pYAXsbsFBC8" data-fancybox>
            <img class="video__thumb" src="<?= get_template_directory_uri(); ?>/assets/img/video-thumb.jpg" alt="Video">
        </a>

    </div>

</section>