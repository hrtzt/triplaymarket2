<div class="categorias-grid">
    
    <?php $cats = get_field( 'home_cats' ); $contador = 0; ?>

    <?php $cats_count = count($cats); ?>

    <?php foreach($cats as $cat) : $contador++; ?>

        <?php
            $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
            $catImage = wp_get_attachment_image_src( $thumbnail_id, 'medium' );
            $image = wp_get_attachment_image_src( $thumbnail_id, 'medium' ); 
        ?>

        <?php if( $contador == 1 ) : ?>
            
            <div class="categories-holder">

        <?php endif; ?>

            <a href="<?= $category_link = $cat->parent > 0 ? get_category_link( $cat->parent ) : get_category_link( $cat->term_id ) ; ?>" class="grid-category">
            
                <div class="grid-category__title">
                    <?php if( $catImage ) : ?>
                        <img class="" src="<?php echo $catImage[0]; ?>" alt="">
                    <?php else : ?>
                        <h1 class="ui-title"><?= $cat->name; ?></h1>
                    <?php endif; ?>
                </div>

                <div class="grid-category__img">
                    <?php if( $image ) : ?>
                        <img width="80" class="" src="<?php echo get_field('cat_featured_img', 'term_' . $cat->term_id); echo $image['url']; ?>" alt="Thumbnail">
                    <?php else : ?>
                        <div class="">
                            <img width="80" class="categoria__img categoria__img_default" src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnail-default-branded2.png" alt="Thumbnail">
                        </div>
                    <?php endif; ?>
                </div>

            </a>

        <?php if( $contador % 3 == 0 && $contador < $cats_count ) : ?>

            </div>

            <div class="categories-holder">

        <?php endif; ?>

    <?php endforeach; ?>

</div>