<?php global $woocommerce; ?>

<aside class="cart-sidebar <?php if( $woocommerce->cart->get_cart_contents_count() == 0 ) : ?> cart-sidebar--empty <?php endif; ?>">

    <?php do_action( 'woocommerce_before_cart' ); ?>

    <?php if( !is_cart() and !is_checkout() ) : ?>

        <?php if( $woocommerce->cart->get_cart_contents_count() > 0 ) : ?>

            <header class="cart-sidebar__header">

                <span class="cart-sidebar__header-close"><i class="fa fa-times-circle"></i></span>

                <div class="wk-cols-me">
                    <div class="cart-sidebar__header-col cart-sidebar__header-col-left wk-col">
                        <h6 class="cart-sidebar__title">Cotización</h6>
                    </div>
                    <div class="cart-sidebar__header-col cart-sidebar__header-col-right wk-col">
                        <span class="cart-sidebar__item-count item-count">
                            <img class="cart-sidebar__item-count-icon item-count__icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-entrega.svg" alt="Items">
                            <span class="cart-sidebar__item-count-number item-count__number"><?=$cart_item_count = $woocommerce->cart->get_cart_contents_count();?></span>
                        </span>
                    </div>
                </div>


            </header>

            <form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
                <?php do_action( 'woocommerce_before_cart_table' ); ?>

                <div class="shop_table_container">
                    <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="product-remove">&nbsp;</th>
                                <th class="product-thumbnail">&nbsp;</th>
                                <th class="product-name"><?php esc_html_e( 'Producto', 'woocommerce' ); ?></th>
                                <th class="product-presentacion"><?php esc_html_e( 'Presentación', 'woocommerce' ); ?></th>
                                <th class="product-medida"><?php esc_html_e( 'Medida', 'woocommerce' ); ?></th>
                                <th class="product-quantity"><?php esc_html_e( 'Cantidad', 'woocommerce' ); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php do_action( 'woocommerce_before_cart_contents' ); ?>
    
                            <?php
                            foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                                $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                                $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
    
                                if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                                    $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                                    ?>
                                    <tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
    
                                        <td class="product-remove">
                                            <a href="<?php echo wc_get_cart_remove_url( $cart_item_key ); ?>" class="remove" aria-label="" data-product_id="" data-product_sku=""><span class="far fa-times-circle"></span></a>
                                            <?php
                                                // echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                                //     '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s"><span class="far fa-times-circle"></span></a>',
                                                //     esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                                                //     __( 'Remove this item', 'woocommerce' ),
                                                //     esc_attr( $product_id ),
                                                //     esc_attr( $_product->get_sku() )
                                                // ), $cart_item_key );
                                            ?>
                                        </td>
    
                                        <td class="product-thumbnail">
                                            <?php
                                            $thumbnail = apply_filters( ' ', $_product->get_image(), $cart_item, $cart_item_key );
    
                                            if ( ! $product_permalink ) {
                                                echo $thumbnail; // PHPCS: XSS ok.
                                            } else {
                                                printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
                                            }
                                            ?>
                                        </td>
    
                                        <td class="product-name" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>">
                                            <?php
                                            if ( ! $product_permalink ) {
                                                echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
                                            } else {
                                                echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
                                            }
    
                                            do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );
    
                                            // Meta data.
                                            //echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.
    
                                            // Backorder notification.
                                            if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                                                echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
                                            }
                                            ?>
    
                                            <?php //print_r($cart_item); ?>
                                            
                                        </td>
    
                                        <td class="product-presentacion" data-title="Presentación">
                                        
                                            <?php echo strtoupper($cart_item['variation']['attribute_pa_presentacion']); ?>
                                            
                                        </td>
                                        <td class="product-medida" data-title="Medida">
                                            
                                            <?php echo $cart_item['variation']['attribute_pa_medida'];  ?>
    
                                        </td>
    
                                        <td class="product-quantity" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>">
                                            <?php
                                            if ( $_product->is_sold_individually() ) {
                                                $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                                            } else {
                                                $product_quantity = woocommerce_quantity_input( array(
                                                    'input_name'   => "cart[{$cart_item_key}][qty]",
                                                    'input_value'  => $cart_item['quantity'],
                                                    'max_value'    => $_product->get_max_purchase_quantity(),
                                                    'min_value'    => '0',
                                                    'product_name' => '',
                                                ), $_product, false );
                                            }
    
                                            echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
    
                            <?php do_action( 'woocommerce_cart_contents' ); ?>
    
                            
                            <?php do_action( 'woocommerce_after_cart_contents' ); ?>
                        </tbody>
                    </table>
                </div>

                <div class="cart_actions">
                    <div class="actions">

                        <button id="update-cart" type="submit" class="button ui-button--outline--black" name="update_cart" value="<?php esc_attr_e( 'Actualizar cotización', 'woocommerce' ); ?>"><?php esc_html_e( 'Actualizar cotización', 'woocommerce' ); ?></button>

                        <?php do_action( 'woocommerce_cart_actions' ); ?>

                        <?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
                    </div>
                </div>

                <?php do_action( 'woocommerce_after_cart_table' ); ?>

            </form>

            <div class="cart-collaterals">
                <?php
                    /**
                     * Cart collaterals hook.
                     *
                     * @hooked woocommerce_cross_sell_display
                     * @hooked woocommerce_cart_totals - 10
                     */
                    do_action( 'woocommerce_cart_collaterals' );
                ?>
            </div>

        <?php endif; ?>
		
    <?php endif; ?>

    <?php do_action( 'woocommerce_after_cart' ); ?>

</aside>