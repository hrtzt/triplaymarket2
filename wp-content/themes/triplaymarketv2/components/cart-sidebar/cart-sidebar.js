$cart_sidebar_height = $('.cart-sidebar').height();
$cart_sidebar_new_position = 'calc( 100% - ' + $cart_sidebar_height + 'px )';
// console.log($cart_sidebar_new_position);

$('.cart-sidebar__header').click(function () {


    if ($('body').hasClass('cart-sidebar--open')) {

        $('body').removeClass('cart-sidebar--open');
        $('.cart-sidebar').css('top', 'calc(100% - 70px)');

    } else {

        $('body').addClass('cart-sidebar--open');

        setTimeout(function () {
            $('.cart-sidebar').css('top', $cart_sidebar_new_position);
        }, 300)

    }
});


/*
 * Abre el carrito desde el icono del menú principal
 * solo si hay items en el carrito, si no da un aviso que esta vacio
 */

    
$('.main-header__cart-count').click(function () {

    if ( ! $('body').hasClass('cart-sidebar--open')) {

        $('body').addClass('cart-sidebar--open');

        setTimeout(function () {
            $('.cart-sidebar').css('top', $cart_sidebar_new_position);
        }, 300)

    }
});

$('#open-cart-menu').click(function () {
    if ($(this).hasClass('main-header__cart-count-empty')) {
        // alert('Aún no haz añadido productos a tu cotización');
        let timerInterval;
        Swal.fire({
            text: 'Aún no has añadido productos a tu cotización',
            timer: 2500,
            showConfirmButton: false,
            customClass: {
                container: 'alert__container',
                content: 'alert__content',
            }
        });
    }
    console.log('algo');
    
});

/*
 * Cierra el cart sidebar en scroll
 */

// $(window).scroll(function(){
//     if ($('body').hasClass('cart-sidebar--open')) {
//         $('body').removeClass('cart-sidebar--open');
//         $('.cart-sidebar').css('top', 'calc(100% - 70px)');
//     }
// });


/*
 * Bloquea el botón de actualizar cotización hasta que 
 * hay algún cambio en los inputs de número
 */

// opacidad como estado inicial
$('#update-cart').css('opacity', '.2');

$('#update-cart').click(function(e){
    e.preventDefault();
});

// Alpha en 1 para indicar que ahora se puede dar click y unbind e preventdefault click
$('.cart-sidebar input.qty').change(function(){
    $(this).parents('.cart-sidebar').find('#update-cart').css('opacity', '1');
    $('#update-cart').unbind('click');
});

