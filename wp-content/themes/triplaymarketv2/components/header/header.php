<?php get_template_part( 'components/searchform/searchform' ); ?>

<?php get_template_part( 'components/absolute/absolute' ); ?>

<section id="main-header" class="main-header wk-section">

    <div class="wk-section-wrap">

        <header class="wk-cols-me">

            <div class="wk-col-2 main-header__logo-container">

                <a class="main-header__logo" href="<?php bloginfo( 'url' ); ?>">

                    <?php if ( get_option('wk_custom_logo_main') ) : ?>

                        <img class="main-header__logo-image" width="240" src="<?php echo get_option( 'wk_custom_logo_main' ); ?>">

                    <?php else : ?>

                        <img class="main-header__logo-image" width="240" src="<?php echo get_template_directory_uri(); ?>/wpkit/admin/img/wpkit-logo.svg">

                    <?php endif; ?>

                </a>

            </div>

            <div class="wk-col-10 wk-d main-header__menu-container">

                <?php

                    $nav_args = array(
                        'theme_location'  => 'main-nav',
                        'menu'            => '',
                        'container'       => 'nav',
                        'container_class' => 'main-header__menu',
                        'container_id'    => '',
                        'menu_class'      => 'main-header__menu-list',
                        'menu_id'         => 'main-header__menu-list',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'before'          => '',
                        'after'           => '',
                        'link_before'     => '',
                        'link_after'      => '',
                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth'           => 0, // Cuantos niveles de jerarquía se incluirán 0 es todos. -1 imprime todos los niveles en uno mismo.
                        'walker'          => ''
                    );

                    wp_nav_menu( $nav_args );

                    /* Para pasar el menu sin los argumentos
                    wp_nav_menu( 'theme_location=header-menu' );
                    */

                ?>

                <?php global $woocommerce; ?>
                <span id="open-cart-menu" class="main-header__cart-count <?= $cart_item_empty = $woocommerce->cart->get_cart_contents_count() > 0 ? '' : 'main-header__cart-count-empty' ?> item-count" title="Cotización">
                    <img class="item-count__icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-entrega.svg" alt="Items">
                    <span class="item-count__number"><?= $woocommerce->cart->get_cart_contents_count();?></span>
                </span>

            </div>

            <div class="wk-col wk-m main-header__offcanvas-icon-container">

                <span class="main-header__offcanvas-icon">

                    <i class="main-header__offcanvas-blocks"></i>

                </span>

            </div>

        </header>

    </div>

</section>

<main>


