$('.slider').slick();

$('.slider-home').slick({
    // slidesToShow: 4,
    fade: true,
    centerMode: false,
    focusOnSelect: true,
    dots: true,
    rows: 0,
    prevArrow: '<span class="slick-prev"><span class="slick-icon"></span></span>',
    nextArrow: '<span class="slick-next"><span class="slick-icon"></span></span>',
});