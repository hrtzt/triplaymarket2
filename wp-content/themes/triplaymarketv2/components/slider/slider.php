<section id="slider" class="wk-section main-video">
    
    <div class="wk-section-wrap">

        <div class="home-slider slider-home">

            <?php if( have_rows( 'home_slider' ) ) : while( have_rows( 'home_slider' ) ) : the_row( 'home_slider' ); ?>
            
                <?php if( get_row_layout() == 'home_slider_video' ) : ?>

                <a class="video" href="<?php the_sub_field( 'home_video' ); ?>" data-fancybox>
            
                    <?php if( get_sub_field( 'home_video_thumbnail' ) ) : ?>
        
                        <img class="video__thumb" src="<?php the_sub_field( 'home_video_thumbnail' ); ?>" alt="Video">
                    
                    <?php else : ?>
                    
                        <img class="video__thumb" src="<?= get_template_directory_uri(); ?>/assets/img/video-thumb.jpg" alt="Video">
                        
                    <?php endif; ?>
                </a>

                <?php else :  ?>

                    <img src="<?php the_sub_field( 'home_banner' ); ?>" alt="<?php bloginfo( 'name' ); ?> | Banner">

                <?php endif; ?>
            
            <?php endwhile; endif; ?>   

        </div>
        
        
    </div>
    
</section>