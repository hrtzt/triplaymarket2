</main>

<section id="main-footer" class="wk-section main-footer">
    
    <div class="wk-section-wrap">

        <div class="main-footer__top">

            <div class="wk-cols">
    
                <div class="wk-col-4 main-footer__col">
                    
                    <h3 class="ui-title main-footer__title">Productos</h3>

                    <nav class="main-footer__menu main-footer__menu-2cols">

                        <ul>

                            <?php $footer_nav_menus = wp_get_nav_menu_items( 'productos'); ?>

                            <?php foreach( $footer_nav_menus as $footer_nav_menu_item ) : ?>

                                <li><a href="<?= $footer_nav_menu_item->url ?>"><?= $footer_nav_menu_item->title ?></a> </li>

                            <?php endforeach; ?>
    
                        </ul>
        
                    </nav>

                </div>
    
                <div class="wk-col-2 main-footer__col">
    
                    <h3 class="ui-title main-footer__title"><?php bloginfo('name'); ?></h3>

                    <nav class="main-footer__menu">

                        <ul>
                            <?php $footer_nav_menus2 = wp_get_nav_menu_items( 'triplaymarket'); ?>

                            <?php foreach( $footer_nav_menus2 as $footer_nav_menu_item ) : ?>

                                <li><a href="<?= $footer_nav_menu_item->url ?>"><?= $footer_nav_menu_item->title ?></a> </li>

                            <?php endforeach; ?>
                        </ul>
        
                    </nav>
    
                </div>
    
                <div class="wk-col-2 main-footer__col">
    
                    <h3 class="ui-title main-footer__title">Redes</h3>

                    <div class="main-footer__menu main-footer__menu-social">
                        <ul>
                            <?php if( get_field( 'info_facebook', 'option' ) ) : ?>
                                <li class="absolute__menu__item"><a target="_blank" href="<?php the_field( 'info_facebook', 'option' ); ?>"><span class="fab fa-facebook-f"></span></a></li>
                            <?php endif; ?>
                            <?php if( get_field( 'info_facebook', 'option' ) ) : ?>
                                <li class="absolute__menu__item"><a target="_blank" href="<?php the_field( 'info_instagram', 'option' ); ?>"><span class="fab fa-instagram"></span></a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
    
                </div>
    
            </div>

        </div>

        <div class="main-footer__bottom">

            <div class="wk-cols">

                <!-- <div class="wk-col-6 main-footer__col">
                    Newletter
                </div> -->

                <div class="wk-col-2 main-footer__col">
                    <?= date('Y') ?> &copy; <?php bloginfo('name'); ?>
                </div>

            </div>

        </div>

            
    </div>

</section>