// Librerías
//@prepros-prepend ../../vendor/fancybox/fancybox.js
//@prepros-prepend ../../vendor/slick/slick.js
//@prepros-prepend ../../vendor/tweenmax/TweenMax.min.js
//@prepros-prepend ../../vendor/scrollto/ScrollToPlugin.min.js
//@prepros-prepend ../../wpkit/js/site.js

// Files
//@prepros-prepend ajax-add-to-cart.js

// Components
//@prepros-prepend ../../components/absolute/absolute.js
//@prepros-prepend ../../components/header/header.js
//@prepros-prepend ../../components/footer/footer.js
//@prepros-prepend ../../components/slider/slider.js
//@prepros-prepend ../../components/offcanvas/offcanvas.js
//@prepros-prepend ../../components/video/video.js
//@prepros-prepend ../../components/categoria/categoria.js
//@prepros-prepend ../../components/brand-strip/brand-strip.js
//@prepros-prepend ../../components/cart-sidebar/cart-sidebar.js
//@prepros-prepend ../../components/mapa-sucursales/mapa-sucursales.js
//@prepros-prepend ../../components/categorias-grid/categorias-grid.js
//@prepros-prepend ../../components/sucursal-row/sucursal-row.js

// Modules
//@prepros-prepend ../../modules/_style-guide/style-guide.js
//@prepros-prepend ../../modules/home/home.js
//@prepros-prepend ../../modules/quienes-somos/quienes-somos.js
//@prepros-prepend ../../modules/productos/productos.js
//@prepros-prepend ../../modules/sucursales/sucursales.js
//@prepros-prepend ../../modules/contacto/contacto.js
//@prepros-prepend ../../modules/search/search.js

$('[data-fancybox]').fancybox({
    toolbar: false,
    smallBtn: true,
    iframe: {
        preload: false
    }
})

