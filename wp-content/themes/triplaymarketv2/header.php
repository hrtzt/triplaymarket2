	<!DOCTYPE html>

	<html lang="<?php bloginfo('language'); ?>" dir="<?php bloginfo('text_direction'); ?>" <?php wk_schema_global_type(); ?> class="<?php wk_user_agent_class(); ?>">

		<head <?php wk_opengraph_header(); ?>>

			<?php wp_head(); ?>

		</head>

		<body <?php body_class('wk-wrap-1200 wk-cols-8'); ?>>

				<!-- <style>
				.site-maintenance {
					position: fixed;
					top: 80px;
					left: 0;
					right: 0;
					margin: auto;
					width: 400px;
					background: red;
					color: white;
					padding: 20px;
					box-shadow: 0px 0px 20px rgba(0,0,0,0.15);
					z-index: 9999999;
				}
			</style>

			<div class="site-maintenance">Este sitio está siendo actualizado, podría no mostrarse de forma correcta. <br> Vuelve a intentarlo más tarde.</div> -->

			<div id="wrapper">

				<?php get_template_part( 'components/header/header' ); ?>

				<?php get_template_part( 'components/offcanvas/offcanvas' ); ?>

				
