<section id="Contacto" class="contacto wk-section">
    <div class="wk-section-wrap">

        <div class="wk-cols">
            <div class="wk-col contacto__col contacto__col-left">
                
                <h4 class="ui-title">LLAMANOS</h4>

                <table class="contacto__table">
                    <tbody>
                        <tr>
                            <td data-label=""><span class="fab fa-whatsapp"></span> Ventas</td>
                            <td class="specs" data-label="">
                                <?php if( have_rows( 'info_tels_contacto_ventas', 'option' ) ) : while( have_rows( 'info_tels_contacto_ventas', 'option' ) ) : the_row( 'info_tels_contacto_ventas', 'option' ); ?>
                                    <a href="tel:<?php the_sub_field( 'info_tel_contacto_ventas', 'option' ); ?>"><strong><?php the_sub_field( 'info_tel_contacto_ventas', 'option' ); ?></strong></a> <br> 
                                <?php endwhile; endif; ?>
                                <p><?php the_field( 'info_leyenda_telefono_de_contacto_de_ventas', 'option' ); ?></p>
                            </td>
                        </tr>
                        <tr>
                            <td data-label="">Corporativo</td>
                            <td class="specs" data-label="">
                                <?php if( have_rows( 'info_tels_contacto_corporativo', 'option' ) ) : while( have_rows( 'info_tels_contacto_corporativo', 'option' ) ) : the_row( 'info_tels_contacto_corporativo', 'option' ); ?>
                                    <a href="tel:<?php the_sub_field( 'info_tel_contacto_corporativo', 'option' ); ?>"><strong><?php the_sub_field( 'info_tel_contacto_corporativo', 'option' ); ?></strong></a> <br> 
                                <?php endwhile; endif; ?>
                                <p><?php the_field( 'info_leyenda_telefono_de_contacto_corporativo', 'option' ); ?></p>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
            <div class="wk-col contacto__col contacto__col-right">
                <h4 class="ui-title">ESCRIBENOS</h4>

                <?php if( get_field( 'info_email', 'option' ) ) : ?>
                    <p class="ui-title-small">Escribenos a <a href="mailto:<?php the_field( 'info_email', 'option' ); ?>"><?php the_field( 'info_email', 'option' ); ?></a> o llena el siguiente formulario.</p>
                <?php endif; ?>


                <?php echo FrmFormsController::get_form_shortcode( array( 'id' => 1, 'title' => false, 'description' => false ) ); ?>
            </div>
        </div>

    </div>
</section>