<section id="quienes-somos" class="quienes-somos wk-section">
    <!-- <img class="quienes-somos__bg" src="<?= get_template_directory_uri(); ?>/assets/img/quienes-somos-bg.jpg" alt="Background"> -->
    

        <article class="article">
        
            <?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

                <?php the_content(); ?>
            
            <?php endwhile; else : echo "Aún no se ha publicado nada"; endif; ?>

        </article>


        <!-- <div class="wk-cols">
            <div class="wk-col quienes-somos__col quienes-somos__col-text quienes-somos__col-left">

                <h1 class="quienes-somos__title"><strong><?php bloginfo('name'); ?></strong></h1>
                <h6 class="quienes-somos__subtitle"><strong>Desde 1985</strong></h6>
                <p>Somos una empresa 100% mexicana con más de 30 años de experiencia y 34 puntos de venta en 13 estados de la República Mexicana</p>

            </div>
            <div class="wk-col quienes-somos__col quienes-somos__col-right">

                <img src="<?= get_template_directory_uri(); ?>/assets/img/quienes-somos-img-1.jpg" alt="Quiénes Somos">

            </div>
        </div> -->

</section>

<!-- <section class="sustentabilidad wk-section">
    <div class="wk-section-wrap">

        <div class="wk-cols">

            <div class="wk-col">

                <img src="<?= get_template_directory_uri(); ?>/assets/img/quienes-somos-img-1.jpg" alt="Quiénes Somos">

            </div>
            <div class="wk-col">

                <div class="sustentabilidad__container">
                    <h3 class="sustentabilidad__title">Sustentabilidad</h3>
                    <p>Nuestros productos cuentan con certificaciones sustentables. Estamos comprometidos con la preservación del medio ambiente.</p>
                </div>

            </div>

        </div>


    </div>
</section>

<section class="cualidades wk-section">
    <div class="wk-section-wrap">

        <div class="wk-cols">

            <div class="wk-col cualidades__col">
                
                <img class="cualidades__icon" src="<?= get_template_directory_uri(); ?>/assets/img/icon-tec.svg" alt="Tecnología de vanguardia">
                <h4 class="cualidades__title">Tecnología de vanguardia</h4>
                <p>Contamos con la tecnología para procesar lorem ipsum dolor sit amet consectetur adipiscing elit, volutpat sagittis tempus donec fames felis risus hac, auctor conubia pretium consequat euismod ultricies.</p>

            </div>

            <div class="wk-col cualidades__col">
                
                <img class="cualidades__icon" src="<?= get_template_directory_uri(); ?>/assets/img/icon-entrega.svg" alt="Entregas a domicilio">
                <h4 class="cualidades__title">Entregas a domicilio</h4>
                <p>No pierdas tiempo recogiendo tus compras, déjanos a nosotros el envío   ipsum dolor sit amet consectetur ad</p>

            </div>

            <div class="wk-col cualidades__col">
                
                <img class="cualidades__icon" src="<?= get_template_directory_uri(); ?>/assets/img/icon-servicio.svg" alt="Vocación de servicio">
                <h4 class="cualidades__title">Vocación de servicio</h4>
                <p>Nuestra principal preocupación es el servicio que reciben  nuestros clientes. Trabajamos todos los días por generar una experiencia de compra que  cumpla con tus estándares.</p>

            </div>

            <div class="wk-col cualidades__col">
                
                <img class="cualidades__icon" src="<?= get_template_directory_uri(); ?>/assets/img/icon-retail.svg" alt="Retail, volumen y calidad">
                <h4 class="cualidades__title">Retail, volumen y calidad</h4>
                <p>Manejamos pedidos pequeños hasta pedidos de gran tamaño. lorem ipsum dolor sit amet consectetur adipiscing elit, volutpat sagittis temp</p>

            </div>

        </div>

    </div>
</section> -->