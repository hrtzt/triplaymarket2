<section class="search wk-section">
    <div class="wk-section-wrap">

        <div class="search-product__wrapper variations-form__wrapper">
            
            <?php //echo get_search_query(); ?>
                    
            <?php if (have_posts()): ?>
    
                <?php while (have_posts()) : the_post(); ?>
    
                    <?php  $product_image_url = has_post_thumbnail() ? wp_get_attachment_url(get_post_thumbnail_id()) : false; ?>
                    
                    <div class="search-product">

                            <h2 class="variations-form__title search-product__title"><?php the_title(); ?></h2> 

                            <div class="search-product__container">
    
                                <div class="search__product-header variations-form__data variations-form__data-image">
                                    <img class="categoria__sample-img search__thumb" src="<?= $product_image_url ? $product_image_url : get_stylesheet_directory_uri() . '/assets/img/thumbnail-default-branded.png' ?>" alt="">
                                </div>
                
                                <div class="search__product-content">
                                    <?php wc_get_template_part( 'content', 'single-product' );?>
                                </div>

                            </div>

                    </div>
                                    
                                        
                <?php endwhile; ?>
    
                <div style="z-index:99999999;" class="pagination"><?php echo paginate_links(); ?></div>
            
                <?php wp_reset_query(); ?>
            
            <?php  else : ?>
    
                <span class="no_content">No hay coincidencias para tu búsqueda.</span>
    
            <?php endif; ?>

        </div>

    </div>
</section>