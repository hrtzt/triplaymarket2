
<?php get_template_part( 'components/slider/slider' ); ?>

<?php get_template_part( 'components/brand-strip/brand-strip' ); ?>

<section class="wk-section" id="cats-grid">
    <div class="wk-section-wrap">
        <?php get_template_part( 'components/categorias-grid/categorias-grid' ); ?>
    </div>
</section>

<?php get_template_part( 'components/mapa-sucursales/mapa-sucursales' ); ?>