<section id="sucursales" class="wk-section sucursales">

    <div class="wk-section-wrap">
        <header id="sucursales-header">
            <div class="headers">
                <div class="headers-labels-titulo">
                    <h1 class="ui-title">Directorio</h1>
                </div>
            </div>
        </header>
        
        <div class="headers-labels-servicios-icons">
            
            <div class="wk-cols-m headers-labels-servicios-icons-container">
                
                <div class="wk-col-6 servicios-icon">
                    <div class="icon-label"><i>Servicio de Corte</i></div>
                    <div class="icon"><i class="fa fa-star-half"></i></div>
                </div>
                <div class="wk-col-6 servicios-icon">
                    <div class="icon-label"><i>Corte 45° cubiertas de cocina</i></div>
                    <div class="icon"><i class="fa fa-cube"></i></div>
                </div>
                <div class="wk-col-6 servicios-icon">
                    <div class="icon-label"><i>Entrega a domicilio</i></div>
                    <div class="icon"><i class="fa fa-truck"></i></div>
                </div>
                <div class="wk-col-6 servicios-icon">
                    <div class="icon-label"><i>Cepillado</i></div>
                    <div class="icon"><i class="fa fa-industry"></i></div>
                </div>
                <div class="wk-col-6 servicios-icon">
                    <div class="icon-label"><i>Estacionamiento</i></div>
                    <div class="icon"><i class="fa fa-car"></i></div>
                </div>
                <div class="wk-col-6 servicios-icon">
                    <div class="icon-label"><i>Pago con tarjeta</i></div>
                    <div class="icon"><i class="fa fa-credit-card"></i></div>
                </div>
            </div>

        </div>
            
        <div class="wk-cols-m headers-labels-servicios">

            <div class="wk-col wk-d headers-label-estado">
                <div class="">Estado</div>
            </div>

            <div class="wk-col headers-label-sucursal">
                <div class="">Sucursal</div>
            </div>

            <div class="wk-col wk-d headers-label">
                <div class="">Dirección</div>
            </div>

            <div class="wk-col wk-d headers-label">
                <div class="">Servicios</div>
            </div>

        </div>

        <?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

            <div class="sucursal__current">
                <?php get_template_part( 'components/sucursal-row/sucursal-row' ); ?>
            </div>
        

        <?php endwhile; else : echo "Aún no se ha publicado nada"; endif; ?>

         <article class="">
            
            <?php $terms = get_terms( array( 'taxonomy' => 'estados', 'hide_empty'   => true, ) ); ?>
                    
            <?php foreach( $terms as $term ) : ?>

                <?php $args = array(
                    'post_type'     => 'sucursales',
                    'tax_query'     => array(
                                        array(
                                            'taxonomy'  => 'estados',
                                            'field'     => 'slug',
                                            'terms'     => $term->slug,
                                        )
                                    ),
                    );
                ?>  
            
                <?php $wp_query = new WP_Query( $args ); ?>

                <?php if( $wp_query->have_posts() ) : while( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

                    <?php get_template_part( 'components/sucursal-row/sucursal-row' ); ?>

                <?php endwhile; wp_reset_postdata(); endif; ?>

            <?php endforeach; ?>

        </article>

    </div>

</section>




