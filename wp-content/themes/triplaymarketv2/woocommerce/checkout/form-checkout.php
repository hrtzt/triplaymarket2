<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}

?>

<h3 class="ui-title-small">Proporciona tus datos para dar seguimiento a la cotización.</h3><br>

<?php 

	if( current_user_can( 'administrator' ) ) {
		?>

			<div class="notice-info">
				<i style="font-size: 32px; opacity: .25;" class="fas fa-info-circle"></i> 
				<p>Para enviar un email de prueba incluye "Test" en el nombre del formulario abajo ("T" mayúscula y "est" en minúsculas.</p>

				<p> El email se enviará a los destinatarios de las sucursales pero sin información de productos y una nota que indica que es un email de prueba.</p>

				<p>Si deseas que este email llegue a tu bandeja, debes incluirte en el destinatario de notificaciones de email global o en los destinatarios de sucursales <a target="_blank" class="ui-text-underline" href="<?php echo admin_url(); ?>/edit-tags.php?taxonomy=estados&post_type=sucursales">aquí.</a>.</p>

				<p><strong>Ves este mensaje porque has iniciado sesión como administrador.</strong></p><br>
			</div>


		<?php 

	}

?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="customer_details_col" id="customer_details">
			<?php do_action( 'woocommerce_checkout_billing' ); ?>
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

		

	<?php endif; ?>
	
	<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>
	
	<h3 id="order_review_heading"><?php esc_html_e( 'Your order', 'woocommerce' ); ?></h3>
	
	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

	<div id="order_review" class="woocommerce-checkout-review-order">
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
		<?php do_action( 'woocommerce_checkout_shipping' ); ?>
	</div>

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
