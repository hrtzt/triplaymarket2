<?php
/*
*
*  Contiene las funciones implementadas en el template.
*
* @package WPKit
* @author ALUMIN
* @version WPKIT 2.0
*/

/*******************************************************************************
WPKit */

	include_once( get_stylesheet_directory() . '/wpkit/config.php' );


/***************************************************************************
* Página de opciones ACF */

	if( function_exists('acf_add_options_page') ) {

		acf_add_options_page(array(
			'page_title' 	=> 'Info | Redes',
			'menu_title'	=> 'TriplayMarket',
			'menu_slug' 	=> 'info-de-tm',
			'capability'	=> 'edit_posts',
			'redirect'		=> false,
			'icon_url'		=> 'dashicons-marker',
		 	'position'		=> '4',
		));

		acf_add_options_page(array(
			'page_title' 	=> 'Registro',
			'menu_title'	=> 'Registro',
			'menu_slug' 	=> 'registro',
			'capability'	=> 'edit_plugins',
			'redirect'		=> false,
			'icon_url'		=> 'dashicons-marker',
		 	'position'		=> '50',
		));

		// acf_add_options_sub_page(array(
		// 	'page_title' 	=> 'Herramientas',
		// 	'menu_title'	=> 'Herramientas',
		// 	'parent_slug'	=> 'opciones-de-panel',
		// ));

		// acf_add_options_sub_page(array(
		// 	'page_title' 	=> 'Opciones',
		// 	'menu_title'	=> 'Opciones',
		// 	'parent_slug'	=> 'options-wpkit',
		// ));

	}

	/* ***********************************************************************************************************************
* Custom post type */

	// 'Custom_post_type_name'

		if ( ! function_exists('wk_custom_post_type') ) {

			// Register Custom Post Type
			function wk_custom_post_type() {

				$ctp_name = 'sucursales';
				$name = 'Sucursales';
				$names = 'Sucursales';

				$labels = array(
					'name'                  => _x( $names, 'Post Type General Name', 'wpkit_text_domain' ),
					'singular_name'         => _x( $name, 'Post Type Singular Name', 'wpkit_text_domain' ),
					'menu_name'             => __( $names, 'wpkit_text_domain' ),
					'name_admin_bar'        => __( $names, 'wpkit_text_domain' ),
					'archives'              => __( $names, 'wpkit_text_domain' ),
					'attributes'            => __( $names . ' Attributes', 'text_domain' ),
					'parent_item_colon'     => __( 'Superior', 'wpkit_text_domain' ),
					'all_items'             => __( $name, 'wpkit_text_domain' ),
					'add_new_item'          => __( 'Añadir', 'wpkit_text_domain' ),
					'add_new'               => __( 'Nuevo', 'wpkit_text_domain' ),
					'new_item'              => __( 'Nuevo', 'wpkit_text_domain' ),
					'edit_item'             => __( 'Editar', 'wpkit_text_domain' ),
					'update_item'           => __( 'Actualizar', 'wpkit_text_domain' ),
					'view_item'             => __( 'Ver', 'wpkit_text_domain' ),
					'search_items'          => __( 'Buscar', 'wpkit_text_domain' ),
					'not_found'             => __( 'No se encontró', 'wpkit_text_domain' ),
					'not_found_in_trash'    => __( 'No se encontró en la papelera', 'wpkit_text_domain' ),
					'featured_image'        => __( 'Imagen destacada', 'wpkit_text_domain' ),
					'set_featured_image'    => __( 'Seleccionar como imagen destacada', 'wpkit_text_domain' ),
					'remove_featured_image' => __( 'Quitar imagen destacada', 'wpkit_text_domain' ),
					'use_featured_image'    => __( 'Usar como imagen destacada', 'wpkit_text_domain' ),
					'insert_into_item'      => __( 'Insertar', 'wpkit_text_domain' ),
					'uploaded_to_this_item' => __( 'Adjuntas a esta publicación', 'wpkit_text_domain' ),
					'items_list'            => __( 'Listado de elementos', 'wpkit_text_domain' ),
					'items_list_navigation' => __( 'Navegación por items', 'wpkit_text_domain' ),
					'filter_items_list'     => __( 'Filtrar por item en el listado', 'wpkit_text_domain' ),
				);
				$rewrite = array(
					'slug'                  => $ctp_name,
					'with_front'            => true,
					'pages'                 => true,
					'feeds'                 => true,
				);
				// Requiere habilitar capabilities para el rol de usuario
				// $capabilities = array(
				// 	'edit_post'          => 'edit_articulo',
				// 	'read_post'          => 'read_articulo',
				// 	'delete_post'        => 'delete_articulo',
				// 	'edit_posts'         => 'edit_articulos',
				// 	'edit_others_posts'  => 'edit_others_articulos',
				// 	'publish_posts'      => 'publish_articulos',
				// 	'read_private_posts' => 'read_private_articulos',
				// 	'create_posts'       => 'edit_articulos',
				// );
				$args = array(
					'label'                 => __( $name, 'wpkit_text_domain' ),
					'description'           => __( 'Publicaciones en el sitio', 'wpkit_text_domain' ),
					'labels'                => $labels,
					'supports'              => array(
													'title',
													//'editor',
													//'excerpt',
													//'author',
													//'thumbnail',
													//'comments',
													//'trackbacks',
													//'revisions',
													//'custom-fields',
													//'page-attributes',
													//'post-formats',
												),
					//'taxonomies'            => array(
					//								'post_tag',
					//								'category'
					//							),
					'hierarchical'          => true,
					'public'                => true,
					'show_ui'               => true,
					'menu_position'         => 5,
					'menu_icon'             => 'dashicons-admin-post',
					'show_in_menu'          => true,
					'show_in_admin_bar'     => true,
					'show_in_nav_menus'     => true,
					'can_export'            => true,
					'has_archive'           => false,
					'exclude_from_search'   => false,
					'publicly_queryable'    => true,
					'rewrite'               => $rewrite,
					// 'capabilities'          => $capabilities,
					'show_in_rest'			=> true,
				);
				register_post_type( $ctp_name, $args );

			}

			add_action( 'init', 'wk_custom_post_type', 0 );

		}

/* ***********************************************************************************************************************
 Taxonomy */

	// Register Custom Taxonomy
	function custom_taxonomy() {

		$labels = array(
			'name'                       => _x( 'Estados', 'Taxonomy General Name', 'text_domain' ),
			'singular_name'              => _x( 'Estado', 'Taxonomy Singular Name', 'text_domain' ),
			'menu_name'                  => __( 'Estados', 'text_domain' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => false,
			'public'                     => true,
			//'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
		);
		register_taxonomy( 'estados', array( 'sucursales' ), $args );

	}
	add_action( 'init', 'custom_taxonomy', 0 );

	
/****************************************************************************************
Sucursales */

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array (
		'key' => 'group_5ab09c884cd48',
		'title' => 'Directorio',
		'fields' => array (
			array (
				'key' => 'field_5ab09d3ce031d',
				'label' => 'Dirección',
				'name' => 'directorio_direccion',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5ab09d70e031e',
				'label' => 'Teléfono',
				'name' => 'directorio_telefonos',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'table',
				'button_label' => '',
				'sub_fields' => array (
					array (
						'key' => 'field_5ab09dbfe031f',
						'label' => 'Prefijo',
						'name' => 'directorio_prefijo',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5ab09de7e0320',
						'label' => 'Teléfono',
						'name' => 'directorio_telefono',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
				),
			),
			array (
				'key' => 'field_5ab0af25ba971',
				'label' => 'Estados',
				'name' => 'directorio_estado',
				'type' => 'taxonomy',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'taxonomy' => 'estados',
				'field_type' => 'select',
				'allow_null' => 0,
				'add_term' => 1,
				'save_terms' => 1,
				'load_terms' => 1,
				'return_format' => 'id',
				'multiple' => 0,
			),
			array (
				'key' => 'field_5ab09e0ee0321',
				'label' => 'Servicios',
				'name' => '',
				'type' => 'tab',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'placement' => 'left',
				'endpoint' => 0,
			),
			array (
				'key' => 'field_5ab0a57ddfdd0',
				'label' => 'Servicios',
				'name' => '',
				'type' => 'message',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => 'Selecciona la casilla de los servicios con los que cuente esta sucursal',
				'new_lines' => 'wpautop',
				'esc_html' => 0,
			),
			array (
				'key' => 'field_5ab09e2fe0322',
				'label' => 'Servicios de corte',
				'name' => 'directorio_servicios_de_corte',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'ui' => 0,
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
			array (
				'key' => 'field_5ab09ea0e0323',
				'label' => 'Corte 45° Cubiertas de cocina',
				'name' => 'directorio_corte45',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'ui' => 0,
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
			array (
				'key' => 'field_5ab09ecee0324',
				'label' => 'Entrega a domicilio',
				'name' => 'directorio_entrega_a_domicilio',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'ui' => 0,
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
			array (
				'key' => 'field_5ab09eeee0325',
				'label' => 'Cepillado',
				'name' => 'directorio_cepillado',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'ui' => 0,
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
			array (
				'key' => 'field_5ab09f06e0326',
				'label' => 'Estacionamiento',
				'name' => 'directorio_estacionamiento',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'ui' => 0,
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
			array (
				'key' => 'field_5ab09f1ae0327',
				'label' => 'Pago con tarjeta',
				'name' => 'directorio_pago_con_targeta',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'ui' => 0,
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'sucursales',
				),
			),
		),
		'menu_order' => 1,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'left',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => 'Directorio administrable',
	));

endif;

/*******************************************************************************
Woocommerce  */

function woocommerce_support() {
	add_theme_support( 'woocommerce');
	// Habilita funciones para la galería con Flexslider, Photoswipe y jQuery Zoom
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}

add_action( 'after_setup_theme', 'woocommerce_support' );

// Deshabilita la hoja de estilos woocommerce.css en la carpeta del plugin
add_filter( 'woocommerce_enqueue_styles', '__return_false' );

/*******************************************************************************
Woocommerce deshabilita validación en ciertos campos en checkout
*
* @see https://docs.woocommerce.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/
* 
*/


add_filter( 'woocommerce_default_address_fields' , 'wpkit_disable_wc_checkout_vals' );
 
function wpkit_disable_wc_checkout_vals( $address_fields ) {
  $address_fields['postcode']['required'] = false;
  $address_fields['last_name']['required'] = false;
  $address_fields['city']['required'] = false;
  $address_fields['country']['required'] = false;
  $address_fields['address_1']['required'] = false;
  $address_fields['address_2']['required'] = false;
  return $address_fields;
}

add_filter( 'woocommerce_checkout_fields' , 'wpkit_remove_wc_checkout_fields' );
 
function wpkit_remove_wc_checkout_fields( $fields ) {
  unset($fields['billing']['billing_postcode']);
  unset($fields['billing']['billing_last_name']);
  unset($fields['billing']['billing_address_1']);
  unset($fields['billing']['billing_address_2']);
  unset($fields['shipping']['billing_last_name']);
  $fields['billing']['billing_phone']['required'] = false;

  return $fields;
}

/*******************************************************************************
Muestra las variaciones en la página de tienda */

/*******************************************************************************
Tus funciones */

/*******************************************************************************
Redirecciona a la página actual al añadir productos */

 add_filter( 'woocommerce_add_to_cart_form_action', 'add_to_cart_variable' );

 function add_to_cart_variable( $product_permalink ){
	 global $wp;
	 $current_page = home_url( add_query_arg( array(), $wp->request ) );
	return _e( $current_page . '/#', 'boton_suscribete' );
	return $product_permalink;
 }

 /*******************************************************************************
 Reemplaza el texto default en el select de variaciones de producto */
 function wpkit_dropdown_variation_options_text($html, $args){
    $html = str_replace('Choose an option', 'Selecciona una opción', $html);
    return $html;
}
add_filter('woocommerce_dropdown_variation_attribute_options_html', 'wpkit_dropdown_variation_options_text', 10, 2);


/*******************************************************************************
Woocommerce ajax */
add_action('wp_ajax_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
        
function woocommerce_ajax_add_to_cart() {

	$product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
	$quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
	$variation_id = absint($_POST['variation_id']);
	$passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
	$product_status = get_post_status($product_id);

	if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status) {

		do_action('woocommerce_ajax_added_to_cart', $product_id);

		if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
			wc_add_to_cart_message(array($product_id => $quantity), true);
		}

		WC_AJAX :: get_refreshed_fragments();
	} else {

		$data = array(
			'error' => true,
			'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id));

		echo wp_send_json($data);
	}

	wp_die();
}


/*********************************************************************************
 * Cambia los textos de la notificaciones de carrito de woocommerce
 */

function tm_add_to_cart_message_html( $message, $products ) {
	$count = 0;
	$titles = array();
	foreach ( $products as $product_id => $qty ) {
		$titles[] = ( $qty > 1 ? absint( $qty ) . ' &times; ' : '' ) . sprintf( _x( '&ldquo;%s&rdquo;', 'Item name in quotes', 'woocommerce' ), strip_tags( get_the_title( $product_id ) ) );
		$count += $qty;
	}
	$titles     = array_filter( $titles );
	$added_text = sprintf( _n(
		'%s se añadió a tu cotización.', // Singular
		'%s se añadió a tu cotización.', // Plural
		$count, // Número de productos añadidos
		'woocommerce' // Textdomain
	), wc_format_list_of_items( $titles ) );
	$message    = sprintf( '<a href="%s" class="button wc-forward">%s</a> %s', esc_url( wc_get_page_permalink( 'cart' ) ), esc_html__( 'View cart', 'woocommerce' ), esc_html( $added_text ) );
	return $message;
}
add_filter( 'wc_add_to_cart_message_html', 'tm_add_to_cart_message_html', 10, 2 );


/*
 * Cambia los textos de woocomerce desde textdomain
 * Es útil para textos estáticos para textos dinámicos se deberá usar 
 * algún hook de woocomerce como wc_add_to_cart_message_html ú otros
 */

function tm_text_domain_texts($translation, $text, $domain) {
	if ($domain == 'woocommerce') {
		if ($text == 'Cart updated.') {
			$translation = 'Cotización actualizada';
        }
		if ($text == 'WooCommerce') {
			$translation = 'Cotizaciones';
        }
		if ($text == 'Orders') {
			$translation = 'Cotizaciones';
        }
	}
	
    return $translation;
}

add_filter('gettext', 'tm_text_domain_texts', 10, 3);
/*******************************************************************************
Elimina el botón me "upload media" en los editores de texto
además oculta el contenedor de cualuqier otro botón de añadir media */

function tm_no_mediaupload_btn(){
    // if ( !current_user_can( 'manage_options' ) ) {
	// }
	remove_action( 'media_buttons', 'media_buttons' );
	echo '
		<style>
			.wp-media-buttons {
				display: none;
			}
		</style>
	';
}
add_action('admin_head', 'tm_no_mediaupload_btn');

/*********************************************************************************
 * Render block
 * 
 * Modifica la forma en que se imprime el contenido desde Guttenberg, 
 * al llamarlo con the_content(), no modifica directamente the_content() 
 * no afecta a get_the_content()
 */

function tm_reder_block_mods( $block_content, $block ) {
		// print_r($block);
		if (	$block['blockName'] === 'core/columns' || 
				$block['blockName'] === 'core/paragraph' || 
				$block['blockName'] === 'core/gallery' || 
				$block['blockName'] === 'core/image' || 
				$block['blockName'] === 'core/heading' ) {
			$content = '<div class="block-section "><div class="block-section-wrap">';
			$content .= $block_content;
			$content .= '</div></div>';
			return $content;
		}
	
	return $block_content;
}
add_filter( 'render_block', 'tm_reder_block_mods', 10, 2 );

/*********************************************************************************
 * Menúes en footer
 */

add_action( 'init', 'tm_more_menues' );

function tm_more_menues() {
	register_nav_menus( array(
		'productos'	=> 'Productos',
		'triplaymarket'	=> 'TriplayMARKET',
	));
}

/*******************************************************************************
 Añade scripts de woocomerce y lo que se necesite */	


function tm_enqueue_woocommerce_scripts() {

	wp_enqueue_script( 'wc-add-to-cart-variation' );
	wp_enqueue_script( 'wc-single-product' );
	wp_enqueue_script( 'wc-add-to-cart-variation' );
	wp_enqueue_script( 'wc-cart-fragments' );
	wp_register_script( 'sweet-alert', 'https://cdn.jsdelivr.net/npm/sweetalert2@9', array(), true );
	wp_enqueue_script( 'sweet-alert' );

}

add_action( 'wp_enqueue_scripts', 'tm_enqueue_woocommerce_scripts' );

function tm_enqueue_admin_scripts() {
	wp_enqueue_script( 'admin-scripts', get_template_directory_uri() . '/assets/js/admin.js', array('jquery'), '1.0', 'true' );
	wp_enqueue_script( 'admin-scripts' );
}

add_action( 'admin_enqueue_scripts', 'tm_enqueue_admin_scripts' );

/*********************************************************************************
 * Cambia el estatus de nuevas cotizaciones / pedidos a En espera al generarse
 */

function tm_estado_inicial_pedido( $order_id ) {
	
	$order = wc_get_order( $order_id );
    $order->update_status( 'on-hold' );
	
}

add_action( 'woocommerce_payment_complete', 'tm_estado_inicial_pedido' );

/*********************************************************************************
 * Emails de notificacion de nueva cotización por ciudad para sucursales
 * 
 */

function conditional_recipient_new_email_notification( $recipient, $order ) {

	// Previene errores en el administrador
	if( ! is_a($order, 'WC_Order') ) return $recipient;

	$estados = array(
		'aguascalientes'		=> 'AG',
		'baja-california'		=> 'BC',
		'baja-california-sur'	=> 'BS',
		'campeche'				=> 'CM',
		'chiapas'				=> 'CS',
		'chiuaua'				=> 'CH',
		'ciudad-de-mexico'		=> 'DF', //REVISAR
		'coahuila'				=> 'CO',
		'colima'				=> 'CL',
		'durango'				=> 'DG',
		'guanajuato'			=> 'GT',
		'guerrero'				=> 'GR',
		'hidalgo'				=> 'HG',
		'jalisco'				=> 'JC',
		'estado-de-mexico'		=> 'MX', // revisar
		'michoacan'				=> 'MI',
		'morelos'				=> 'MO',
		'nayarit'				=> 'NA',
		'nuevo-leon'			=> 'NL',
		'oaxaca'				=> 'OA',
		'puebla'				=> 'PU',
		'queretaro'				=> 'QT',
		'quintana-roo'			=> 'QR',
		'san-luis-potosi'		=> 'SL',
		'sinaloa'				=> 'SI',
		'sonora'				=> 'SO',
		'tabasco'				=> 'TB',
		'tamaulipas'			=> 'TM',
		'tlaxcala'				=> 'TL',
		'veracruz'				=> 'VE',
		'yucatan'				=> 'YU',
		'zacatecas'				=> 'ZA',
	);

	// Obtiene las siglas 2 digitos del estado desde la orden de compra
	// ESTO CAUSA UN ERROR EN EL PANEL DE ADMINISTRACIÓN AUNQUE SI HACE LOS ENVÍOS DE NOTIFICACIONES
	// BIEN, NO LO HE PODIDO RESOLVER
	$estado_siglas = method_exists( $order, 'get_billing_state()') ? $order->get_billing_state() : $order->get_billing_state();
	// $estado_siglas = $order->get_billing_state();
	
	// Busca a partir de las siglas el nombre completo del estado en el arreglo $estados
	$estado_slug = array_search( $estado_siglas, $estados );
	
	// Obtiene el id del término basado en el nombre del estado
	$estado_id = get_term_by( 'slug', $estado_slug, 'estados' )->term_id;
	
	// Obtiene el campo de emails en la taxonomía de estados
	$addr_email  = get_field( 'sucursal_email', 'estados_' . $estado_id ); 
	
	// Añade el contenido del campo 'sucursal_email' al destinatario del correo
	// $recipient .= ', triplaymarket@alumin.agency';
	$recipient .= ', ' . $addr_email; 

    return $recipient;
}

add_filter( 'woocommerce_email_recipient_new_order', 'conditional_recipient_new_email_notification', 10, 2 );


/*********************************************************************************
 * Añade emails adicionales a la cabezera de recepción de emails de woocommerce
 */

 add_filter( 'woocommerce_email_headers', 'tm_emails_adicionales_envio_wc', 9999, 3 );
 
function tm_emails_adicionales_envio_wc( $headers, $email_id, $order ) {
    if ( 'customer_completed_order' == $email_id ) {
        //$headers .= "Cc: Name <your@email.com>" . "\r\n"; 
    }
	$headers .= "Bcc: Alumin <notificaciones.alumin@gmail.com>" . "\r\n"; 
    return $headers;
}



/*********************************************************************************
 * Snippets
 */

/* ***********************************************************************************************************************
* Oculta items de menu en admin menu*/

	function remove_menu_items_wpkit() {
		remove_menu_page('edit.php');
		remove_menu_page('edit-comments.php');
	}
	add_action('admin_menu', 'remove_menu_items_wpkit');

function so_39252649_remove_processing_status( $statuses ){
    if( isset( $statuses['wc-processing'] ) ){
        unset( $statuses['wc-processing'] );
        unset( $statuses['wc-pending'] );
        unset( $statuses['wc-cancelled'] );
        unset( $statuses['wc-refunded'] );
        unset( $statuses['wc-failed'] );
    }
    return $statuses;
}
add_filter( 'wc_order_statuses', 'so_39252649_remove_processing_status' );

