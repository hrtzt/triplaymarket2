<?php if( get_option( 'option_private_site' ) ) { if( ! is_user_logged_in() ) { get_template_part( 'wpkit/inc/login' ); return true; } }

/**
*
* Página 
*
* @package WPKit
* @author ALUMIN
* @copyright Copyright (C) Alumin.Agency
* @version WPKIT 3.0
*
*/

get_header(); 

	if( is_page('quienes-somos') ) {
		get_template_part('modules/quienes-somos/quienes-somos'); 
	} elseif ( is_page('productos') ) {
		get_template_part('modules/productos/productos'); 
	} elseif ( is_page('sucursales') ) {
		get_template_part('modules/sucursales/sucursales');
	} elseif ( is_page('contacto') ) {
		get_template_part('modules/contacto/contacto');
	} elseif ( is_page('inicio') ) {
		get_template_part('modules/home/home');
	} else {

		if(have_posts()) : 
			
			while(have_posts()) : the_post();

				echo '<main class="wk-section">';

					echo '<div class="wk-section-wrap">';

						the_content();
					
					echo '</div>';

				echo '</main>';

			endwhile;

		endif;
		
	}

get_footer(); ?>